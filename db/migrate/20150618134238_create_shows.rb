class CreateShows < ActiveRecord::Migration
  def change
    create_table :shows do |t|
      t.integer :band_id
      t.string :local
      t.datetime :date
      t.string :description

      t.timestamps
    end
  end
end
