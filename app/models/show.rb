class Show < ActiveRecord::Base
	belongs_to :band

	validates :band_id, :local, :date, :description, presence: true
	validates :local, length: { maximum: 200 }
	validates :description, length: { maximum: 400 }
	validate :date_is_valid_datetime

	scope :band_id, -> (band_id) { where band_id: band_id }
  scope :local, -> (local) { where("local like ?", "%#{local}%")}

  def date_is_valid_datetime
  	if (date)
  	if date < Date.today
	    errors.add(:date, "Seu show não pode ocorrer anterior a #{Date.today.strftime("%d/%m/%Y")}")
		  end
	  end
	end

end
