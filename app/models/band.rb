class Band < ActiveRecord::Base
	has_many :shows
	validates :name, :category, presence: true
end
