class ShowsController < ApplicationController
  before_action :set_show, only: [:show, :edit, :update, :destroy]

  # GET /shows
  def index
    
    @bands = Band.all.order(:name)
    
    if params[:s] == "all"
      @shows = Show.all.order(:date)
    else
      @shows = Show.where(:date => Time.zone.now..(Time.zone.now + 30.days)).order(:date)
    end

    filtering_params(params).each do |key, value|
      @shows = @shows.public_send(key, value) if value.present?
    end
  end

  # GET /shows/1
  def show
  end

  # GET /shows/new
  def new
    @show = Show.new
    @bands = Band.all.order(:name)
  end

  # GET /shows/1/edit
  def edit
  end

  # POST /shows
  def create
    @bands = Band.all.order(:name)
    @show = Show.new(show_params)

    if @show.save
      redirect_to @show, notice: 'Show criado com sucesso!'
    else
      render action: 'new'
    end
  end

  # PATCH/PUT /shows/1
  def update
    if @show.update(show_params)
      redirect_to @show, notice: 'Show was successfully updated.'
    else
      render action: 'edit'
    end
  end

  # DELETE /shows/1
  def destroy
    @show.destroy
    redirect_to shows_url, notice: 'Show was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_show
      @show = Show.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def show_params
      params.require(:show).permit(:band_id, :local, :date, :description)
    end
    def filtering_params(params)
      params.slice(:local, :band_id)
    end
end
